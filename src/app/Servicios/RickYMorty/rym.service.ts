import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RymService {

  public URI = 'https://rickandmortyapi.com/api';

  constructor(
    // : significa del tipo
    private _http : HttpClient,
    
    ) { }

    getPersonajes(){
      // `para concatenar variables`
      const url = `${ this.URI }/character`;
      return this._http.get( url );
    }
}

