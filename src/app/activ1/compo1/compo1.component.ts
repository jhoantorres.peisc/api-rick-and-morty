import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-compo1',
  templateUrl: './compo1.component.html',
  styleUrls: ['./compo1.component.css']
})
export class Compo1Component implements OnInit{

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    
  }


  //arrays = [1, 2, 3, 4];
  arr1 = [1, 2, 3, 4];
  arr2 = ["a", "e", "i", "o", "u"];

  //objetos = {propiedades : valor}
  obj1 = {
    hambre: "come",
    sed: "bebe",
    sueno: "duerme",
  }
  obj2 = {
    hambre: "no come",
    sed: "no bebe",
    sueno: "no duerme",
  }

  //arraysdeobjetos = [{propiedades: val
  arrobj = [{
    compu: 1,
    edo: "encendida",
    marca: "asus",
   },{
    compu: 2,
    edo: "apagada",
    marca: "hp",
   },{
    compu: 3,
    edo: "encendida",
    marca: "dell",
   },
  ]
  
  color = 'azul';

  numero= '1';
}
