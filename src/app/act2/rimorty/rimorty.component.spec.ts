import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RimortyComponent } from './rimorty.component';

describe('RimortyComponent', () => {
  let component: RimortyComponent;
  let fixture: ComponentFixture<RimortyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RimortyComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RimortyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
