import { Component, OnInit } from '@angular/core';
import { RymService } from 'src/app/Servicios/RickYMorty/rym.service';

@Component({
  selector: 'app-rimorty',
  templateUrl: './rimorty.component.html',
  styleUrls: ['./rimorty.component.css']
})
export class RimortyComponent implements OnInit{

  public personajes : any [] = [];

  constructor( private _rym : RymService ){

  }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    
    this.getPersonajes();

  }

  getPersonajes(){
    this._rym.getPersonajes()
    .subscribe((data:any) => {
      console.log(data);

      this.personajes = data.results;
    });
  }
}
